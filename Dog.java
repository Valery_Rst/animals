package net.animals;

public class Dog extends Animals {
    public Dog(String nameOfAnimal, int feelingOfSatiety, int feelingOfFatigue) {
        super(nameOfAnimal, feelingOfSatiety, feelingOfFatigue);
    }

    @Override
    public void play() {
        setFeelingOfFatigue(getFeelingOfFatigue() - (int) (10 + Math.random()*8));
        setFeelingOfSatiety(getFeelingOfSatiety() - (int)(7 + Math.random()*13));
        System.out.println("Хозяин! Это было весело! Гав-гав");
    }

    @Override
    public  void  sleep() {
        setFeelingOfFatigue(getFeelingOfFatigue() + (int)(10 + Math.random()*5));
        setFeelingOfSatiety(getFeelingOfSatiety() - (int)(7 + Math.random()*7));
        System.out.println("Хозяин! Хочу спать! Гав-гав");
    }

    @Override
    public void eat() {
        setFeelingOfFatigue(getFeelingOfFatigue() + (int)(10 + Math.random()*8));
        setFeelingOfSatiety(getFeelingOfSatiety() + (int)(20 + Math.random()*10));
        System.out.println("Хозяин! Хочу есть! Гав-гав");
    }

    @Override
    public String toString() {
        return getNameOfAnimal() + " устал на " + getFeelingOfFatigue() + "% и сыт на " + getFeelingOfSatiety() + "%";
    }
}
