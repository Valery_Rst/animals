package net.animals;

public abstract class Animals {
    private String nameOfAnimal; //имя животного
    private int feelingOfSatiety; //чувство сытости
    private int feelingOfFatigue; //чувство усталости

    public Animals(String nameOfAnimal, int feelingOfSatiety, int feelingOfFatigue) {
        this.nameOfAnimal = nameOfAnimal;
        this.feelingOfSatiety = feelingOfSatiety;
        this.feelingOfFatigue = feelingOfFatigue;
    }

    abstract void play();
    abstract void sleep();
    abstract void eat();

    public void setFeelingOfSatiety(int feelingOfSatiety) {

        this.feelingOfSatiety = feelingOfSatiety;
    }

    public int getFeelingOfSatiety() {
        return feelingOfSatiety;
    }

    public void setFeelingOfFatigue(int feelingOfFatigue) {
        this.feelingOfFatigue = feelingOfFatigue;
    }

    public int getFeelingOfFatigue() {
        return feelingOfFatigue;
    }

    public void setNameOfAnimal(String nameOfAnimal) {
        this.nameOfAnimal = nameOfAnimal;
    }

    public String getNameOfAnimal() {
        return nameOfAnimal;
    }
}
