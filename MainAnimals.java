package net.animals;

public class MainAnimals {
    public static void main(String[] args) {
        Cat cat = new Cat("Жора ", 35, 65);
        Dog dog = new Dog("Витя ", 70, 30);
        Parrot parrot = new Parrot("Георгий ", 50, 45);

        Animals[] animals = new Animals[]{cat, dog, parrot};

        for (int i = 6; i < 20; i++) {
            System.out.println("Время " + i + ": 00");
            for (int z = 0; z < 3; z++) {
                if (animals[z].getFeelingOfSatiety() < 25) {
                    animals[z].eat();
                    System.out.println(animals[z].getNameOfAnimal() + "проголодался. Я покормил его");
                } else if (animals[z].getFeelingOfFatigue() < 25) {
                    animals[z].sleep();
                    System.out.println(animals[z].getNameOfAnimal() + "устал. Я положил его спать");
                } else {
                    animals[z].play();
                    System.out.println(animals[z].getNameOfAnimal() + "отлично поиграл.");
                }
            }
        }
        isHungry(animals);

    }

    public static void isHungry(Animals[] animals) {
        if (animals[0].getFeelingOfSatiety() < animals[1].getFeelingOfSatiety() && animals[0].getFeelingOfSatiety() < animals[2].getFeelingOfSatiety()) {
            System.out.println("Самый голодный " + animals[0].toString());
        }

        if (animals[1].getFeelingOfSatiety() < animals[0].getFeelingOfSatiety() && animals[1].getFeelingOfSatiety() < animals[2].getFeelingOfSatiety()) {
            System.out.println("Самый голодный " + animals[1].toString());
        }

        if ((animals[2].getFeelingOfSatiety() < animals[1].getFeelingOfSatiety() && animals[2].getFeelingOfSatiety() < animals[0].getFeelingOfSatiety())) {
            System.out.println("Самый голодный " + animals[2].toString());
        }
    }
}
